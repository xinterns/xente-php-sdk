<?php

namespace XentePaymentSDK\Services;

class Constant 
{
    // Constant base domain
    private $baseDomain = 'api.xente.co';

    // Variable URLs
    public $baseUrl;
    public $authUrl;
    public $transactionUrl;
    public $accountUrl;
    public $paymentProvidersUrl;

    public function __construct($isSandbox) 
    {
        $baseDomain = 'api.xente.co';
        $baseSandboxDomain = 'sandbox.' . $baseDomain;
        
        if ($isSandbox == true) {
            $baseDomain = $baseSandboxDomain;
        }
    
        $this->baseUrl = 'https://' . $baseDomain . '/api/v1';

        // $baseUrl . '/Auth/login'; Will be used instead
        $this->authUrl = 'http://34.90.206.233:83/api/v1/Auth/login';

        // $baseUrl . '/transactions'; Will be used instead
        $this->transactionUrl = 'http://34.90.206.233:83/api/v1/transactions';

        // This will be $baseUrl . '/Accounts';
        $this->accountUrl = 'http://34.90.206.233:83/api/v1/Accounts';

        // This will be $baseUrl . '/paymentproviders'
        $this->paymentProvidersUrl =
        'http://34.90.206.233:83/api/v1/paymentproviders';
    }
}

